﻿namespace Post
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TextboxDescricao = new System.Windows.Forms.RichTextBox();
            this.TextboxTitulo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonPost = new System.Windows.Forms.Button();
            this.ButtonLike = new System.Windows.Forms.Button();
            this.ButtonDislike = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.TextboxData = new System.Windows.Forms.TextBox();
            this.LabelContLike = new System.Windows.Forms.Label();
            this.LabelContDislike = new System.Windows.Forms.Label();
            this.TextboxSaida = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextboxDescricao
            // 
            this.TextboxDescricao.Location = new System.Drawing.Point(22, 152);
            this.TextboxDescricao.Name = "TextboxDescricao";
            this.TextboxDescricao.Size = new System.Drawing.Size(253, 154);
            this.TextboxDescricao.TabIndex = 1;
            this.TextboxDescricao.Text = "";
            // 
            // TextboxTitulo
            // 
            this.TextboxTitulo.Location = new System.Drawing.Point(22, 90);
            this.TextboxTitulo.Name = "TextboxTitulo";
            this.TextboxTitulo.Size = new System.Drawing.Size(253, 20);
            this.TextboxTitulo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(124, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Titulo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(102, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Descrição";
            // 
            // ButtonPost
            // 
            this.ButtonPost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonPost.Location = new System.Drawing.Point(63, 361);
            this.ButtonPost.Name = "ButtonPost";
            this.ButtonPost.Size = new System.Drawing.Size(167, 23);
            this.ButtonPost.TabIndex = 6;
            this.ButtonPost.Text = "Post";
            this.ButtonPost.UseVisualStyleBackColor = true;
            this.ButtonPost.Click += new System.EventHandler(this.ButtonPost_Click);
            // 
            // ButtonLike
            // 
            this.ButtonLike.Enabled = false;
            this.ButtonLike.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonLike.Image = ((System.Drawing.Image)(resources.GetObject("ButtonLike.Image")));
            this.ButtonLike.Location = new System.Drawing.Point(398, 373);
            this.ButtonLike.Name = "ButtonLike";
            this.ButtonLike.Size = new System.Drawing.Size(60, 58);
            this.ButtonLike.TabIndex = 7;
            this.ButtonLike.UseVisualStyleBackColor = true;
            this.ButtonLike.Click += new System.EventHandler(this.ButtonLike_Click);
            // 
            // ButtonDislike
            // 
            this.ButtonDislike.Enabled = false;
            this.ButtonDislike.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDislike.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDislike.Image")));
            this.ButtonDislike.Location = new System.Drawing.Point(474, 373);
            this.ButtonDislike.Name = "ButtonDislike";
            this.ButtonDislike.Size = new System.Drawing.Size(58, 58);
            this.ButtonDislike.TabIndex = 8;
            this.ButtonDislike.UseVisualStyleBackColor = true;
            this.ButtonDislike.Click += new System.EventHandler(this.ButtonDislike_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Data";
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(43, 325);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(19, 15);
            this.LabelStatus.TabIndex = 10;
            this.LabelStatus.Text = "...";
            // 
            // TextboxData
            // 
            this.TextboxData.Enabled = false;
            this.TextboxData.Location = new System.Drawing.Point(23, 28);
            this.TextboxData.Name = "TextboxData";
            this.TextboxData.Size = new System.Drawing.Size(110, 20);
            this.TextboxData.TabIndex = 11;
            // 
            // LabelContLike
            // 
            this.LabelContLike.AutoSize = true;
            this.LabelContLike.Location = new System.Drawing.Point(423, 443);
            this.LabelContLike.Name = "LabelContLike";
            this.LabelContLike.Size = new System.Drawing.Size(16, 13);
            this.LabelContLike.TabIndex = 12;
            this.LabelContLike.Text = "...";
            // 
            // LabelContDislike
            // 
            this.LabelContDislike.AutoSize = true;
            this.LabelContDislike.Location = new System.Drawing.Point(497, 443);
            this.LabelContDislike.Name = "LabelContDislike";
            this.LabelContDislike.Size = new System.Drawing.Size(16, 13);
            this.LabelContDislike.TabIndex = 13;
            this.LabelContDislike.Text = "...";
            // 
            // TextboxSaida
            // 
            this.TextboxSaida.BackColor = System.Drawing.SystemColors.Window;
            this.TextboxSaida.Enabled = false;
            this.TextboxSaida.Location = new System.Drawing.Point(333, 28);
            this.TextboxSaida.Name = "TextboxSaida";
            this.TextboxSaida.Size = new System.Drawing.Size(257, 328);
            this.TextboxSaida.TabIndex = 14;
            this.TextboxSaida.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(439, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Output";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 465);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextboxSaida);
            this.Controls.Add(this.LabelContDislike);
            this.Controls.Add(this.LabelContLike);
            this.Controls.Add(this.TextboxData);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ButtonDislike);
            this.Controls.Add(this.ButtonLike);
            this.Controls.Add(this.ButtonPost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextboxTitulo);
            this.Controls.Add(this.TextboxDescricao);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox TextboxDescricao;
        private System.Windows.Forms.TextBox TextboxTitulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonPost;
        private System.Windows.Forms.Button ButtonLike;
        private System.Windows.Forms.Button ButtonDislike;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.TextBox TextboxData;
        private System.Windows.Forms.Label LabelContLike;
        private System.Windows.Forms.Label LabelContDislike;
        private System.Windows.Forms.RichTextBox TextboxSaida;
        private System.Windows.Forms.Label label4;
    }
}

