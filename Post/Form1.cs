﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Post
{
    public partial class Form1 : Form
    {
        Post Post = new Post();

        public Form1()
        {
            InitializeComponent();
        }
        private void ButtonPost_Click(object sender, EventArgs e)
        {
            string retorno = String.Empty, retorno2 = String.Empty;            
            retorno = Post.verify(TextboxTitulo.Text, "Introduza um Titulo");
            retorno2 = Post.verify(TextboxDescricao.Text, "Introduza uma Descrição");
            LabelStatus.Text = retorno + "\n" + retorno2;
            if (retorno == String.Empty && retorno2 == String.Empty)
            {
                ButtonLike.Enabled = true;
                ButtonDislike.Enabled = true;
                Post.Titulo = TextboxTitulo.Text;
                Post.Descriçao = TextboxDescricao.Text;
                Post.Data = DateTime.Now;
                TextboxData.Text = Post.Data.ToString();
                LabelStatus.Text = Post.mensagempost();
                TextboxSaida.Text = Post.Data.ToString() + "\n\n" + Post.Titulo.ToString() + "\n\n" + Post.Descriçao.ToString();
            }
            

        }

        private void ButtonLike_Click(object sender, EventArgs e)
        {
            Post.mensagemlike();
            LabelStatus.Text = Post.mensagemlike();
            LabelContLike.Text = Post.contadorlikes().ToString();
        }

        private void ButtonDislike_Click(object sender, EventArgs e)
        {
            Post.mensagemdislike();
            LabelStatus.Text = Post.mensagemdislike();
            LabelContDislike.Text = Post.contadordislikes().ToString();
        }
    }
}
