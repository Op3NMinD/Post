﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post
{
    public class Post
    {
        int likes = 0,dislikes = 0;
        #region Atributos
        private string _titulo;
        private string _descriçao;
        private DateTime _data;
        private string _mensagem;
        #endregion

        #region Propriedades
        public string Titulo
        {
            get { return _titulo; }
            set { _titulo = value; }
        }

        public string Descriçao
        {
            get { return _descriçao; }
            set { _descriçao = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Mensagem
        {
            get { return _mensagem; }
            set { _mensagem = value; }
        }
        #endregion

        #region Métodos

        public Post () : this("??", "??",DateTime.Now) { }
        //public Post() { }
        //public Post(Post post)
        //{
        //    Titulo = post.Titulo;
        //    Descriçao = post.Descriçao;
        //    Data = post.Data;
        //    Mensagem = post.Mensagem;
        //}

        public Post(Post post):this(post.Titulo, post.Descriçao, post.Data) { }

        public Post(string titulo, string descriçao, DateTime data)
        {
            Titulo = titulo;
            Descriçao = descriçao;
            Data = data;           
        }
        
        public string mensagempost()
        {
            Mensagem = "Post efetuado com sucesso";
            return Mensagem;
        }
        public string mensagemlike()
        {
            Mensagem = "Gostas deste post!";                       
            return Mensagem;
        }
        public string mensagemdislike()
        {
            Mensagem = "Já não gostas deste post!";
            return Mensagem;
        }
        public int contadorlikes()
        {
            likes++;
            return likes;
        }        
        public int contadordislikes()
        {
            dislikes++;
            return dislikes;
        }

        public static string verify(string textoValidar, String msg)
        {
            string aux = String.Empty;
            if (String.IsNullOrEmpty(textoValidar))
            {
                aux = msg;
            }
            return aux;
        }
        #endregion
    }
}
